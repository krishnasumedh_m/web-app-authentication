Some key implementation considerations -

-> This is an authentication web-app built using using Nodejs and Expressjs.

-> The UI layer of the web-app is composed of login, registration pages, user_account access pages etc.

-> The web-app makes calls/requests using Axios to the RESTful Authentication API (https://bitbucket.org/krishnasumedh_m/authentication-web-service)
   to validate it's user's credentials.

-> Upon successful validation, the user is redirected to their home page.(Working on extending it to provide personalized suggestions/updates to the user.)

-> Used Cookies to store auth_tokens issued by the Web service when the user provides valid credentials.
   This session is retained in the browser for a certain time_out period.

-> Generated html on the server by using the Mustache.js templating system.

-> Also performed client-side validations on email, password and other user inputs before calling the API. Took all possible scenarios
 into consideration and handled them.  Validation for both login and registration uses a common data-driven approach.

-> Used regex's for validating Password's such that they contain atleast 8 characters excluding whitespace characters and
   atleast one digit

  -----------------------------

  Update->

   In order to avoid frustrating user experiences,the application is designed to not loose any user input. When re-displaying a
   page with error message(s) after an error, the redisplayed page will retain any previously provided user input (the sole exception
   to this would be for fields with sensitive information like passwords).