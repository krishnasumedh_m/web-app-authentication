'use strict';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const axios = require('axios');

function Users(WS_URL) {
    this.baseUrl = WS_URL;
}

//All action functions return promises.
Users.prototype.login = function(app,email,pwd) {
    return axios.put(`${this.baseUrl}/users/${email}/auth`,{pw:pwd}, { maxRedirects: 0 })
        .then((response) => {
            //console.log("response ",response);
             var resp;
            if(response.status === 200)
            {
                resp = response.data;
                //console.log("resp",resp);
            }
            return resp;})
    .catch((error) => {
            if(error.response !== undefined)
            {
                if(error.response.status ===404 || error.response.status === 401 )
                    return Promise.reject(error.response.status);
                else
                    return Promise.reject(error);
            }
         else
            {
                 console.log("error in login : ",error);
                 return Promise.reject(error.response);
            }
    });
}

//*************************************************************************

Users.prototype.newUser = function(app,request,response) {
    const userInfo ={ firstname:request.body.firstname, lastname:request.body.lastname , email:request.body.email };
    return axios.put(`${this.baseUrl}/users/${request.body.email}?pw=${request.body.pwd}`,userInfo, { maxRedirects: 0 }) //, { maxRedirects: 0 }
        .then((response) => {
            //console.log("response: ",response);
            var resp;
            if(response.status === 201)
            {
                resp = response.data;
            }
            return resp;})
      .catch((error) => {
           if(error.response !==undefined)
             {
                 if (error.response.status === 303) {
                     return Promise.reject(error.response.status);
                 }
                 else
                 {
                     return Promise.reject(error);
                 }
             }
             else
            {
                 return Promise.reject(error.response);
            }
       });
}

//*************************************************************************

Users.prototype.getAccountDetails = function(id,authToken){
    return axios.get(`${this.baseUrl}/users/${id}`, { headers: { Authorization: `Bearer ${authToken}`} })
        .then((response)=>{
             var resp;
            if(response.status === 200) {
                resp = response.data;
                //console.log("resp",resp);
            }
            return resp;
            })
    .catch((error) =>{
        if(error.response.status){
        return Promise.reject(error.response.status);
        }
        else{
            console.log("Error in retreiving details : ",error);
            return Promise.reject(error);
        }
     });
}

module.exports = {
    Users:Users
};