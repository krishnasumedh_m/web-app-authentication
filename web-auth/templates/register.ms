<html>
<!DOCTYPE html>

  <head>
    <title>Registration Form</title>
    <link rel="stylesheet" href="/css/style.css">
  </head>

  <body>
  <div id="form">
        <h1>Registration Form</h1>

          <form name="registerationform" action="/registeration" method="post">
              <label>
                First Name*
              </label>
              <input type="text" name="firstname" value="{{firstname}}" required autocomplete="on" placeholder="First Name" />
             <br>

            <br>
              <label>
                Last Name*
              </label>
              <input type="text" name="lastname" value="{{lastname}}" required autocomplete="on" placeholder="Last Name"/>
              <br>

             <br>
            <label>
              Email Address*
            </label>
            <input type="email" name="email" value="{{email}}" required autocomplete="on" placeholder="Email Address"/>
            <br>
            {{#userError}}<span class="error">{{userError}}<br></span>{{/userError}}<br>

           <br>
            <label>
              Password*
            </label>
             <input type="password" name="pwd" value="" required autocomplete="off" placeholder="Enter Password"/>
            <br>
            {{#pwdError}}<span class="error">{{pwdError}}<br></span>{{/pwdError}}<br>

            <br>
            <label>
              Confirm Password*
            </label>
            <input type="password" name="confirmpwd" value="" required autocomplete="off" placeholder="Confirm Password" />
            <br>
            {{#pwdmatchError}}<span class="error">{{pwdmatchError}}<br></span>{{/pwdmatchError}}<br>
            <p> {{#webServiceerror}}<span class="error">{{webServiceerror}}</span>{{/webServiceerror}}</p>

            <br> <input type="submit" name="register" value="Register" class="button button-block"/> </input>


          </form>
          <a href="/login"> Login Page </a>

   </div>
</body>

</html>