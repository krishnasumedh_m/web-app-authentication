<html>
<!DOCTYPE html>
  <head>
    <title>Login Form</title>
    <link rel="stylesheet" href="/css/style.css">
  </head>


  <body>
        <div class="login">
          <h1>Login page!</h1>

          <form name="loginform" action="/userlogin" method="POST">

            <div id="util">
            <label>
            Email Address*
            </label>
            <input type="email" name="email" value="{{email}}" required autocomplete="off" placeholder="Email Address"/><br>
            {{#emailError}}<span class="error">{{emailError}}</span>{{/emailError}}
            {{#notFound}}<span class="error">{{notFound}}</span>{{/notFound}}<br><br>
            </div>

          <div id="util1">
           <label>
            Password*
            </label>
            <input type="password" name="pwd" value="" required autocomplete="off" placeholder="Enter Password"/>
            {{#pwdError}}<span class="error">{{pwdError}}</span>{{/pwdError}}
            <p>{{#authError}}<span class="error">{{authError}}<br></span>{{/authError}}</p>
            </div>
            <p class="forgot"><a href="#">Forgot Password?</a></p>

            <p> <a href="register"> Registration Page </a> </p>

          <input type="submit" value="login" class="button button-block"/> </input>
          <br><br>
         {{#webServiceerror}}<span class="error">{{webServiceerror}}</span>{{/webServiceerror}}
          </form>

        </div>
   </body>
   </html>