#!/usr/bin/env nodejs

'use strict';
const assert = require('assert');
const path = require('path');
const process = require('process');
const minimist = require('minimist');

const OPTS = [
    // ['t', 'auth-time' ],
       ['d', 'ssl-dir' ]
];
//const DEFAULT_AUTH_TIMEOUT = 300;
const DEFAULT_SSL_DIR = '.';

function usage(prg) {
    const opts = OPTS.map(function(opt) {
        console.log("  ",opt[1]);
        const value = opt[1].replace('-', '_').toUpperCase();
        return `[ -${opt[0]}|--${opt[1]} ${value} ]`
    });
    console.error(`usage: ${path.basename(prg)} ${opts.join(' ')} PORT WS_URL`);
    process.exit(1);
}
//if -- given before any CLI, then not stored in the _ array.  If -- not given then everything comes under the __ array.

function getOptions(argv) {
    const opts0 = OPTS.reduce((a, b) => a.concat(b), []);
    //console.log("opts0: ",opts0);
    const opts = minimist(argv.slice(2));
    //console.log("opts: ",opts); console.log(opts._.length);

    if (opts._.length !== 2)
    { // if port or WS_URL not given
        usage(argv[1]);
    }
    let port;
    if(!(port = Number(opts._[0]))) // a string value in  port will give port value as NaN
    {
        console.log("Invalid Port");
        process.exit(1);
    }

    for (let k of Object.keys(opts)) {
        if (k === '_') continue;
        if (opts0.indexOf(k) < 0)
        {
            console.error(`bad option '${k}'`);
            usage(argv[1]);
        }
    }
    return {
        port: opts._[0],
       // authTimeout: opts.t || opts['auth-time'] || DEFAULT_AUTH_TIMEOUT,
        sslDir: opts.d || opts['ssl-dir'] || DEFAULT_SSL_DIR,
        WS_URL:opts._[1]
    };
}

module.exports = {
    options: getOptions(process.argv)
};


if (!module.parent) {
    console.log(getOptions(process.argv));
}


