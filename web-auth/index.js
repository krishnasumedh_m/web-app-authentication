#!/usr/bin/env nodejs
'use strict';
const options = require('./options').options;
console.log(options);

//nodejs dependencies
const fs = require('fs');
const process = require('process');

//external dependencies
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mustache = require('mustache');
const https = require('https');
const http  = require('http');

//local dependencies
const user = require('./users/users');

const STATIC_DIR = 'statics';
const TEMPLATES_DIR = 'templates';
//const USER_COOKIE = 'userId';
var USER_COOKIE ;

/*************************** Route Handling ****************************/
function setupRoutes(app) {

     app.get('/', rootRedirectHandler(app));
     app.get('/login', loginpageHandler(app));
     app.get('/register', registerationpageHandler(app));
     app.post('/userlogin', loginHandler(app));
     app.post('/registeration', registerationHandler(app));
     app.get('/user/accounts',userAccount(app));
     app.post('/logout', logoutHandler(app));
}

function rootRedirectHandler(app) {
    return function(req, res) {
        res.redirect('/user/accounts');
    };
}


function logoutHandler(app) {
    return function(request, response) {
       let userID;
       let authToken;
        for(var id in request.cookies){
            userID =id;
            authToken  = request.cookies[id];
        }
        response.cookie(userID, authToken, {expires: new Date(0)});
        //response.send("<h1>Logout Successful !</h1>");
        response.redirect("/login");
    }
}

function loginpageHandler(app) {
    return function(request, response) {
        response.send(doMustache(app, 'login', {}));
    }
}

function registerationpageHandler(app) {
    return function(request, response) {
        response.send(doMustache(app, 'register', {}));
    }
}
//**************** userAccount module ******************************

function userAccount(app) {
    return function(request, response) {
       // const authToken = request.cookie[USER_COOKIE];
        if((Object.keys(request.cookies).length) !==0)
        {
            //console.log(request.cookies);
            let authToken;
            let userID;
            for(var id in request.cookies){
                userID =id;
                authToken  = request.cookies[id];
            }
                app.users.getAccountDetails(userID,authToken)
                .then((data)=>{
                    data.user = data.firstname+"  "+data.lastname;
                    //console.log(data);
                    response.send(doMustache(app, 'account',data)); })
               .catch((error)=>{
                    if(error === 401 || error === 404)
                    {
                        response.redirect('/login');
                    }
              });
        }
        else
        {   response.redirect('/login');
        }
    }
}

//**************** registerationHandler module ******************************

function registerationHandler(app) {
    return function(request, response) {

        let er = [];
        ['firstname','lastname','email'].forEach((name) => {
            const val = request.body[name];
            if (typeof val === 'undefined' || val.trim().length === 0) {
            //er.push({ msg: `Please enter the ${name}.` });
                var errors =
                    {
                    firstname:request.body.firstname, lastname:request.body.lastname,email:request.body.email };

            response.send(doMustache(app, 'register', errors));
            }
    });
        if (er.length > 0) {
            errorPage(app, er, response);
        }
        const emailValid =  /([a-zA-Z0-9._-]+@(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))/gi.test(request.body.email);
        const pwdValid =  /^(?=.*[0-9])(?=.*[!@#$%^&*]*)[^\s]{8,}$/g.test(request.body.pwd);
        const USER_COOKIE = request.body.email;
        let userPromise = null;

        if(pwdValid && emailValid && request.body.pwd === request.body.confirmpwd )
        {
            if (typeof userCookie === 'undefined') {
                app.users.newUser(app, request, response)
                    .then((data) => {
                    return data.authToken;
                   }).
                then(function (authToken) {
                    response.cookie(USER_COOKIE, authToken, {maxAge: 86400 * 1000, secure: true});
                    response.redirect('/user/accounts');})
                .catch((err) => {
                        var errors = {
                        firstname: request.body.firstname, lastname: request.body.lastname, email: request.body.email };
                    if (err !==undefined) {
                        if (err === 303) {
                            errors.userError = 'User already registered !';
                            response.send(doMustache(app, 'register', errors));
                        }
                        else
                            console.error("regHandler error ", err)
                    }
                    else {
                        errors.webServiceerror = "Registeration Web Service Error  ! Please try again after some time";
                        response.send(doMustache(app, 'register', errors));
                       }
            });
            }
           else
            {    //ccokie exists. User already registered.
                const errors = { userError: 'User already registered !' };
                response.send(doMustache(app, 'register', errors));
            }
        }
        else
        {
                var errors = {
                firstname:request.body.firstname, lastname:request.body.lastname,email:request.body.email};

                if(!pwdValid && !emailValid && request.body.pwd !== request.body.confirmpwd )
                {
                    errors.emailError ='Invalid email';
                    errors.pwdError = 'Password should contain atleast 8 characters excluding whitespace characters and atleast one digit';
                    errors.pwdmatchError ='The passwords do not match.';
                    response.send(doMustache(app, 'register', errors));
                }
                else if(!pwdValid)
                {   //Password not according to the requirements
                    errors.pwdError =  'Password should contain atleast 8 characters excluding whitespace characters and atleast one digit';
                    response.send(doMustache(app, 'register', errors));
                }
                else if(!emailValid)
                {   //Password not according to the requirements
                    errors.emailError =  'Invalid email';
                    response.send(doMustache(app, 'register', errors));
                }
                else if(!pwdValid && !emailValid)
                {   //Password && email not according to the requirements
                    errors.emailError = 'Invalid email';
                    errors.pwdError = 'Password should contain atleast 8 characters excluding whitespace characters and atleast one digit';
                    response.send(doMustache(app, 'register', errors));
                }
                else if(!pwdValid && request.body.pwd !== request.body.confirmpwd)
                {   //Password invalid && do not match
                    errors.pwdError = 'Password should contain atleast 8 characters excluding whitespace characters and atleast one digit';
                    errors.pwdmatchError ='The passwords do not match.';
                    response.send(doMustache(app, 'register', errors));
                }
                else if(!emailValid && request.body.pwd !== request.body.confirmpwd)
                { //invalid email && passwords do not match
                    errors.emailError = 'Invalid email';
                    errors.pwdmatchError= 'The passwords do not match.';
                    response.send(doMustache(app, 'register', errors));
                }
                else if(request.body.pwd !== request.body.confirmpwd)
                {   //Passwords do not match
                    errors.pwdmatchError = 'The passwords do not match.';
                    response.send(doMustache(app, 'register', errors));
                }
                else
                {
                    console.log("Registration error : missed case")
                }
        }
    }
}

//****************  loginHandler module  ******************************
function loginHandler(app) {
    return function(request, response) {
        let errors = [];
        ['email'].forEach((name) => {
            const val = request.body[name];
            if (typeof val === 'undefined' || val.trim().length === 0) {
              errors.push({ msg: `Please enter the ${name}.` });
            }
        });

        const valid =  /([a-zA-Z0-9._-]+@(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))/gi.test(request.body.email);
        const pValid =  /^(?=.*[0-9])(?=.*[!@#$%^&*]*)[^\s]{8,}$/g.test(request.body.pwd);

        if (errors.length > 0) {
            errorPage(app, errors, response);
        }
        else if(valid && pValid)
        {
            const email = request.body.email;
            const pwd = request.body.pwd;
            const USER_COOKIE = request.body.email;
                   //console.log( "USER_COOKIE: ",USER_COOKIE);
                   //console.log(request.cookies);

            let userPromise = null;
                     app.users.login(app,email,pwd)
                    .then((data)=> {
                        return data.authToken; })
                    .then((authToken)=> {
                        response.cookie(USER_COOKIE, authToken, { maxAge: 86400*1000,secure:true});
                        response.redirect('/user/accounts');})
                   .catch((error) => {
                            //console.log("Error Occurred");
                        var errors = {email: request.body.email};
                        if(error !==undefined)
                        {
                             if (error === 404) {
                                errors.notFound = 'User not found';
                                 response.send(doMustache(app, 'login', errors));
                                 }
                                 else if (error === 401) {
                                 errors.authError = "Invalid password";
                                 response.send(doMustache(app, 'login', errors));
                                 }
                                 else {
                                console.log(error);
                                 }
                         }
                         else
                            {
                                errors.webServiceerror = "Web Service Error ! Please try again after some time";
                                response.send(doMustache(app, 'login', errors));
                            }
                        });
        }
        else
        {
            var clienterror = { email:request.body.email };

            if(!pValid && !valid)
            {   //Password && email not according to the requirements
                clienterror.emailError = 'Invalid email';
                clienterror.pwdError = 'Password should contain atleast 8 characters excluding whitespace characters and atleast one digit';
                response.send(doMustache(app, 'login', clienterror));
            }
            else if(!valid)
            {
                //template for invalid emailID.
                clienterror.emailError = 'Please provide a valid email-ID';
                response.send(doMustache(app, 'login', clienterror));
            }
            else if(!pValid)
            {
                //Password not according to the requirements
                clienterror.pwdError =  'Password should contain atleast 8 characters excluding whitespace characters and atleast one digit';
                response.send(doMustache(app, 'login', clienterror));
            }

        }
    }
}

/************************ Utility functions ****************************/
function doMustache(app, templateId, view) {
    return mustache.render(app.templates[templateId], view);
}

function errorPage(app, errors, response) {
    if (!Array.isArray(errors)) errors = [ errors ];
    const html = doMustache(app, 'errors', { errors: errors });
    response.send(html);
}

/*************************** Initialization ****************************/

function setupTemplates(app) {
    app.templates = {};
    for (let fname of fs.readdirSync(TEMPLATES_DIR)) {
        const m = fname.match(/^([\w\-]+)\.ms$/);
        //console.log(fname,m);
        if (!m) continue;
        try {
            app.templates[m[1]] =
                String(fs.readFileSync(`${TEMPLATES_DIR}/${fname}`));
                //console.log(app.templates[m[1]]);
        }
        catch (e) {
            console.error(`cannot read ${fname}: ${e}`);
            process.exit(1);
        }
    }
}

function setup() {
    process.chdir(__dirname);
    //const port = getPort(process.argv);
    const KEY_PATH = options.sslDir;
    const CERT_PATH = options.sslDir;
    const port = options.port;
    const WS_URL  = options.WS_URL;
    const app = express();
    app.use(cookieParser());
    setupTemplates(app);
    app.use(express.static(STATIC_DIR));
    app.use(bodyParser.urlencoded({extended: true}));
    const users = new user.Users(WS_URL);
    app.users = users;
    setupRoutes(app);

    https.createServer({
        key: fs.readFileSync(KEY_PATH+"/key.pem"), // KEY_PATH
        cert: fs.readFileSync(CERT_PATH+"/cert.pem")},app).listen(port,function() {
        console.log(`listening on port ${port}`);
        }); //CERT_PATH

    /*app.listen(port, function() {
        console.log(`listening on port ${port}`);
    });*/
}

setup();
